import 'package:flutter_app/model/home_model.dart';
import 'package:flutter_app/request/config.dart';
import 'package:flutter_app/request/request.dart';

class HomeRequest {
  static Future<List<MovieItem>> requestMovieList(int start) async {
    final movieURL =
        'subject_collection/movie_showing/items?start=$start&count=${HomeConfig.movieCount}&apiKey=054022eaeae0b00e0fc068c0c0a2102a';
    //发送网络请求,获取数据
    final result = await MyHttp.request(movieURL);
    final subject = result['subject_collection_items'];
    int? total = result['total'];
    //将Map转成Model
    List<MovieItem> movies = [];
    for (var item in subject) {
      try {
        movies.add(MovieItem.fromMap(item, total));
      } catch (e) {
        print(e);
      }
    }
    return movies;
  }
}
