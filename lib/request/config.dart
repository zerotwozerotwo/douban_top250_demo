class MyHttpConfig {
  static final String baseURL = "https://frodo.douban.com/api/v2/";
  static final int timeout = 5000;
}

class HomeConfig{
  static final int movieCount = 20;
}
