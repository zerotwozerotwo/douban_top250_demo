import 'package:flutter/material.dart';

class MyMallContent extends StatefulWidget {
  @override
  _MyMallContentState createState() => _MyMallContentState();
}

class _MyMallContentState extends State<MyMallContent> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "市集",
        style: TextStyle(fontSize: 30, color: Colors.green),
      ),
    );
  }
}
