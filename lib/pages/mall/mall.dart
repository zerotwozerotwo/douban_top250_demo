import 'package:flutter/material.dart';
import 'package:flutter_app/pages/mall/mall_content.dart';

class MyMall extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("市集")),
      ),
      body: MyMallContent(),
    );
  }
}
