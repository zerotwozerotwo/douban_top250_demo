import 'package:flutter/material.dart';
import 'package:flutter_app/pages/profile/profile_content.dart';

class MyProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("我的")),
      ),
      body: MyProfileContent(),
    );
  }
}
