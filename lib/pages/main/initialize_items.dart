import 'package:flutter/material.dart';
import 'package:flutter_app/pages/group/group.dart';
import 'package:flutter_app/pages/home/home.dart';
import 'package:flutter_app/pages/mall/mall.dart';
import 'package:flutter_app/pages/profile/profile.dart';
import 'package:flutter_app/pages/subject/subject.dart';
import 'bottom_bar_item.dart';

List<MyBottomBarItem> items = [
  MyBottomBarItem('home', '首页'),
  MyBottomBarItem('subject', '书影音'),
  MyBottomBarItem('group', '小组'),
  MyBottomBarItem('mall', '市集'),
  MyBottomBarItem('profile', '我的'),
];

List<Widget> pages = [
  MyHomePage(),
  MySubject(),
  MyGroup(),
  MyMall(),
  MyProfile(),
];
