import 'package:flutter/material.dart';
import 'package:flutter_app/pages/group/group_content.dart';

class MyGroup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("小组")),
      ),
      body: MyGroupContent(),
    );
  }
}
