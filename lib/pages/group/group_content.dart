import 'package:flutter/material.dart';

class MyGroupContent extends StatefulWidget {
  @override
  _MyGroupContentState createState() => _MyGroupContentState();
}

class _MyGroupContentState extends State<MyGroupContent> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "小组",
        style: TextStyle(fontSize: 30, color: Colors.green),
      ),
    );
  }
}
