import 'package:flutter/material.dart';
import 'package:flutter_app/model/home_model.dart';
import 'package:flutter_app/pages/home/home_movie_item.dart';
import 'package:flutter_app/request/home_request.dart';

class MyHomeContent extends StatefulWidget {
  @override
  _MyHomeContentState createState() => _MyHomeContentState();
}

class _MyHomeContentState extends State<MyHomeContent> {
  final List<MovieItem> movies = [];
  int page = 0;
  bool flag = false;

  @override
  void initState() {
    super.initState();
    HomeRequest.requestMovieList(page * 20).then((res) {
      setState(() {
        movies.addAll(res);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return NotificationListener(
      onNotification: buildNotification,
      child: ListView.builder(
        itemCount: movies.length,
        itemBuilder: (context, index) {
          return MyHomeMovieItem(movies[index]);
        },
      ),
    );
  }

  bool buildNotification(ScrollNotification notification) {
    if (movies.length == movies[0].total) {
      return true;
    }
    if (notification.metrics.atEdge &&
        notification.metrics.pixels > 0 &&
        !flag) {
      setState(() {
        page++;
        flag = true;
      });
      print("end");
      HomeRequest.requestMovieList(page * 20).then((res) {
        setState(() {
          movies.addAll(res);
          flag = false;
        });
      });
    }
    return true;
  }
}
