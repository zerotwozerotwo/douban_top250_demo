import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/model/home_model.dart';
import 'package:flutter_app/widgets/mydashed.dart';
import 'package:flutter_app/widgets/star_rating.dart';

class MyHomeMovieItem extends StatelessWidget {
  final MovieItem movie;

  MyHomeMovieItem(this.movie);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 8, color: Color(0xffcccccc)),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          buildHeader(),
          SizedBox(height: 6),
          buildContent(),
          SizedBox(height: 6),
          buildFooter(),
        ],
      ),
    );
  }

  //1.头部的排名
  Widget buildHeader() {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 238, 205, 144),
        borderRadius: BorderRadius.circular(3),
      ),
      child: Text(
        "No.${movie.rank}",
        style: TextStyle(
          fontSize: 18,
          color: Color.fromARGB(255, 131, 95, 36),
        ),
      ),
    );
  }

  //2.内容布局
  Widget buildContent() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        buildContentImage(),
        SizedBox(width: 8),
        Expanded(
          child: IntrinsicHeight(
            child: Row(
              children: <Widget>[
                buildContentInfo(),
                SizedBox(width: 8),
                buildContentDashLine(),
                SizedBox(width: 8),
                buildContentWish(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget buildContentImage() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(3),
      child: Image.network(
        movie.imageURL!,
        height: 150,
      ),
    );
  }

  Widget buildContentInfo() {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          buildContentInfoTitle(),
          buildContentInfoRate(),
          buildContentInfoDesc(),
        ],
      ),
    );
  }

  Widget buildContentInfoTitle() {
    return Text.rich(
      TextSpan(
        children: [
          WidgetSpan(
            child: Icon(
              Icons.play_circle_outline_outlined,
              color: Colors.redAccent,
              size: 30,
            ),
            baseline: TextBaseline.ideographic,
            alignment: PlaceholderAlignment.middle,
          ),
          TextSpan(
            text: movie.title,
            style: TextStyle(fontSize: 15),
          ),
          WidgetSpan(
            child: Text(
              "(${movie.playDate})",
              style: TextStyle(fontSize: 13, color: Colors.grey),
            ),
            alignment: PlaceholderAlignment.middle,
          )
        ],
      ),
    );
  }

  Widget buildContentInfoRate() {
    return FittedBox(
      child: Row(
        children: <Widget>[
          MyRating(
            rating: movie.rating,
            size: 15,
          ),
        ],
      ),
    );
  }

  Widget buildContentInfoDesc() {
    return Text(
      movie.subTitle!,
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(fontSize: 12),
    );
  }

  Widget buildContentDashLine() {
    return Container(
      height: 100,
      child: MyDashLine(
        color: Colors.pink,
        dashedWidth: .4,
        axis: Axis.vertical,
        dashedHeight: 6,
        count: 10,
      ),
    );
  }

  Widget buildContentWish() {
    return Container(
      height: 100,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset("assets/images/home/wish.png"),
          Text(
            "想看",
            style: TextStyle(
              fontSize: 14,
              color: Color.fromARGB(255, 235, 170, 60),
            ),
          )
        ],
      ),
    );
  }

  Widget buildFooter() {
    return Container(
      padding: EdgeInsets.fromLTRB(5, 2, 5, 2),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Color(0xffe2e2e2),
        borderRadius: BorderRadius.circular(3),
      ),
      child: Text(
        movie.title!,
        style: TextStyle(
          fontSize: 15,
          color: Color(0xff666666),
        ),
      ),
    );
  }
}
