import 'package:flutter/material.dart';
import 'package:flutter_app/pages/subject/subject_content.dart';

class MySubject extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("书影音")),
      ),
      body: MySubjectContent(),
    );
  }
}
