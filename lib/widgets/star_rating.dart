import 'package:flutter/material.dart';

class MyRating extends StatelessWidget {
  final double? rating;
  final double maxRating;
  final int count;
  final double size;
  final Color unselectedColor;
  final Color selectedColor;
  final Widget unselectedImage;
  final Widget selectedImage;

  MyRating({
    required this.rating,
    this.maxRating = 10,
    this.count = 5,
    this.size = 30,
    this.unselectedColor = const Color(0xffbbbbbb),
    this.selectedColor = const Color(0xffff0000),
    Widget? unselectedImage,
    Widget? selectedImage,
  })  : this.unselectedImage = unselectedImage ??
      Icon(Icons.star_border, color: unselectedColor, size: size),
        this.selectedImage =
            selectedImage ?? Icon(Icons.star, color: selectedColor, size: size);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: buildRating(),
    );
  }

  List<Widget> buildRating() {
    return <Widget>[
      Row(mainAxisSize: MainAxisSize.min, children: buildUnselectedStar()),
      Row(mainAxisSize: MainAxisSize.min, children: buildSelectedStar())
    ];
  }

  List<Widget> buildSelectedStar() {
    //1.创建stars
    List<Widget> stars = [];
    final star = Icon(Icons.star, color: this.selectedColor, size: this.size);

    //2.构建满的star
    double oneValue = this.maxRating / this.count;
    int entireCount = (this.rating! / oneValue).floor();
    for (var i = 0; i < entireCount; i++) {
      stars.add(star);
    }
    //3.构建剩余star
    double leftWidth = ((this.rating! / oneValue) - entireCount) * this.size;
    final halfStar = ClipRect(
      child: star,
      clipper: MyStarClipper(leftWidth),
    );
    stars.add(halfStar);

    if (stars.length > this.count) {
      return stars.sublist(0, this.count);
    }
    return stars;
  }

  List<Widget> buildUnselectedStar() {
    return List.generate(this.count, (index) {
      return Icon(
        Icons.star_border,
        color: this.unselectedColor,
        size: this.size,
      );
    });
  }
}

class MyStarClipper extends CustomClipper<Rect> {
  double width;

  MyStarClipper(this.width);

  @override
  Rect getClip(Size size) {
    return Rect.fromLTRB(0, 0, this.width, size.height);
  }

  @override
  bool shouldReclip(MyStarClipper oldClipper) {
    return oldClipper.width != this.width;
  }
}