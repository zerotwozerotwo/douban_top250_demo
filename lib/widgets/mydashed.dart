import 'package:flutter/material.dart';

class MyDashLine extends StatelessWidget {
  final Axis axis;
  final double dashedWidth;
  final double dashedHeight;
  final int count;
  final Color color;

  MyDashLine({
    this.axis = Axis.horizontal,
    this.dashedWidth = 5,
    this.dashedHeight = 5,
    this.count = 10,
    this.color = Colors.redAccent,
  });

  @override
  Widget build(BuildContext context) {
    return Flex(
      direction: this.axis,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: List.generate(this.count, (_) {
        return SizedBox(
          width: this.dashedWidth,
          height: this.dashedHeight,
          child: DecoratedBox(
            decoration: BoxDecoration(color: this.color),
          ),
        );
      }),
    );
  }
}
