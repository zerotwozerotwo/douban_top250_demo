int counter = 1;

class MovieItem {
  int? rank;
  String? imageURL;
  String? title;
  String? subTitle;
  String? playDate;
  double? rating;
  List<String>? casts;
  String? director;
  int? total;

  @override
  String toString() {
    return 'MovieItem{rank: $rank, imageURL: $imageURL, title: $title, subTitle: $subTitle, playDate: $playDate, rating: $rating, casts: $casts, director: $director}';
  }

  MovieItem.fromMap(Map<String, dynamic> json, int? total) {
    if (json['directors'].length == 0) {
      this.director = "暂无导演";
    } else {
      this.director = json['directors'][0];
    }
    this.rank = counter++;
    if (json['rating'] == null) {
      this.rating = 3;
    } else {
      this.rating = json['rating']['value'];
    }
    this.imageURL = json['cover']['url'];
    this.title = json['title'];
    this.subTitle = json['card_subtitle'];
    this.playDate = json['year'];
    this.casts = json['actors'].cast<String>();
    this.total = total;
  }
}
